#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MainPlayerState.generated.h"

UCLASS()
class BEESWAX_API AMainPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	FORCEINLINE int GetHealth() const { return Health; }
	FORCEINLINE void SetHealth(int value) { Health = value; }

	FORCEINLINE int GetMaxHealth() const { return MaxHealth; }
	FORCEINLINE void SetMaxHealth(int value) { MaxHealth = value; }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health", Replicated, Meta=(ClampMin=0))
	int Health{ 0 };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Health", Replicated, Meta=(ClampMin=0))
	int MaxHealth{ 0 };
};
